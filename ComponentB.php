<?php


class ComponentB
{

    /**
     * @var ComponentA
     */
    protected $componentA;

    /**
     * @var ComponentC
     */
    protected $componentC;

    /**
     * @param \ComponentA $componentA
     * @return \ComponentA
     */
    public function setComponentA($componentA)
    {
        $this->componentA = $componentA;
        return $this;
    }

    /**
     * @return \ComponentA
     */
    public function getComponentA()
    {
        return $this->componentA;
    }

    /**
     * @param \ComponentC $componentC
     * @return \ComponentC
     */
    public function setComponentC($componentC)
    {
        $this->componentC = $componentC;
        return $this;
    }

    /**
     * @return \ComponentC
     */
    public function getComponentC()
    {
        return $this->componentC;
    }


    public function doB()
    {
        echo 'B doing something' . PHP_EOL;
        $this->componentA->doAB();
        $this->componentC->doCB();
    }

    public function doBA()
    {
        echo 'B doing something requested from A' . PHP_EOL;
    }
} 