<?php


class ComponentC
{
    public function doCA()
    {
        echo 'C doing something requested from A' . PHP_EOL;
    }

    public function doCB()
    {
        echo 'C doing something requested from B' . PHP_EOL;
    }
} 