<?php


class ComponentA
{

    /**
     * @var ComponentB
     */
    protected $componentB;

    /**
     * @var ComponentC
     */
    protected $componentC;

    function __construct($componentB, $componentC)
    {
        $this->componentB = $componentB;
        $this->componentC = $componentC;
    }


    public function doA()
    {
        echo 'A done something' . PHP_EOL;
        $this->componentB->doBA();
        $this->componentC->doCA();
    }

    public function doAB()
    {
        echo 'A doing something requested from B' . PHP_EOL;
    }


} 